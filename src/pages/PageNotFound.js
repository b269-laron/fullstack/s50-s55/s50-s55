import {Button, Row, Col} from 'react-bootstrap'
import Nav from 'react-bootstrap/Nav';

import {NavLink} from 'react-router-dom';

export default function PageNotFound() {
return (
    <Row>
    	<Col className="p-5">
            <h1>Error 404 - Page not found</h1>
            <p>The page you are looking for cannot be found</p>
            <Button variant="primary">
            	<Nav.Link as={NavLink} to="/">Back to Home</Nav.Link>
            </Button>
        </Col>
    </Row>
	)
};