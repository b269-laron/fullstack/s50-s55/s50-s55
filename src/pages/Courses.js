//import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard';
import {useState, useEffect} from 'react';

export default function Courses() {
	//console.log(coursesData);
	// const courses  = coursesData.map(course => {
	// 	return (
	// 		<CourseCard key={course.id} course = {course} />
	// 		)
	// })


	// to store the courses retrieved from database
	const [courses, setCourses] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/active`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setCourses(data.map(course => {
				return(
					<CourseCard key={course._id} course={course} />
				)
			}))
		})
	}, [])


	return (
		<>
		{courses}
		</>
	)	
}

		/*Way to declare prop drilling*/
		/*We can pass information from one component to another using props (props drilling*/
		/*Curly brases {} are used for props to signify that we are providing passing information from one component to another using JS Expression*/