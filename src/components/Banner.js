import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

/* [ALTERNATIVE IMPORTING CODE]
import { Button, Row, Component} from 'react-bootstrap';
*/

export default function Banner() {
return (
    <Row>
    	<Col className="p-5">
            <h1>Zuitt Coding Bootcamp</h1>
            <p>Opportunities for everyone, everywhere.</p>
            <Button variant="primary">Enroll now!</Button>
        </Col>
    </Row>
	)
};