import {useState, useEffect} from 'react';

import { Button, Row, Col, Card } from 'react-bootstrap';

import {Link} from 'react-router-dom';

export default function CourseCard({course}) {


    const { name, description, price, _id } = course;
   

    // const [count, setCount] = useState(0);
    // const [seats, setSeats] = useState(30);

    // function enroll() {
    //     setCount(count + 1) 
    // };

    //function enroll() {
        // if (seats > 0) {
        //     setCount(count + 1);
        //     console.log("Enrollees: " + count);
        //     setSeats(seats - 1);
        //     console.log("Seats: " + seats);
        // } else {
        //     alert('No more seats available')
        // }
        //setCount(count + 1);
        //setSeats(seats - 1);
    //};

//useEffect(() => {
    //if(seats <= 0){
        //alert('No more seats available')
    //}
//}, [seats]);

    // function enrollees () {
    //     slotsCount(seats - 1)
    //         if (slotsCount = 0){
    //             alert("No more seats")
    //         }     
    // }



return (
    <Row className="mt-3 mb-3">
            <Col xs={12}>
                <Card className="cardHighlight p-0">
                    <Card.Body>
                        <Card.Title><h4>{name}</h4></Card.Title>
                        <Card.Subtitle>Description</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Price</Card.Subtitle>
                        <Card.Text>{price}</Card.Text>
                        {/*<Card.Subtitle>Count: {count}</Card.Subtitle>
                        <Card.Subtitle>Seats: {seats}</Card.Subtitle>*/}
                        {/*<Button variant="primary" onClick={enroll} disabled={seats<=0}>Enroll</Button>*/}
                        <Button className="bg-primary" as={Link} to={`/courses/${_id}`}>Details</Button>
                    </Card.Body>
                </Card>
            </Col>
    </Row>        
    )
}

/*[ ACTIVITY ]*/
// import Button from 'react-bootstrap/Button';
// import Card from 'react-bootstrap/Card';

// export default function CourseCard() {
//   return (
//     <Card>
//       <Card.Body>
//         <Card.Title>Full-stack Development</Card.Title>
//         <h6>Description</h6>
//         <Card.Text>
//           Complete your skill-set by combining your back-end know-how with an understanding of client and server interaction. Integrate your server into a reactive single page application, in an end-to-end experience of how modern web applications are built.
//         </Card.Text>
//         <h6>Price:</h6>
//         <Card.Text>
//           PhP 40,000
//         </Card.Text>
//         <Button variant="primary">Enroll</Button>
//       </Card.Body>
//     </Card>
//   );
// }

//export default BasicExample;